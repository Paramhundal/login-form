//
//  ViewController.m
//  form
//
//  Created by CLI112 on 9/17/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

UILabel *firstname1;
UILabel *lastname1;
UILabel *email1;
UILabel *phonenumber1;
UILabel *password1;
UILabel *confirmpassword1;

UITextField *firstname;
UITextField *lastname;
UITextField *email;
UITextField *phonenumber;
UITextField *password;
UITextField *confirmpassword;


UIButton *button1;
- (void)viewDidLoad {
    [super viewDidLoad];
   
    firstname1=[[UILabel alloc]init];
    firstname1.frame=CGRectMake(20, 50, 130, 80);
    firstname1.textColor=[UIColor redColor];
    firstname1.text=@"FIRST NAME";
    [self.view addSubview:firstname1];
    
    
    lastname1=[[UILabel alloc]init];
    lastname1.frame=CGRectMake(20, 130, 130, 80);
    lastname1.textColor=[UIColor redColor];
    lastname1.text=@"LAST NAME";
    [self.view addSubview:lastname1];
    
    
    email1=[[UILabel alloc]init];
    email1.frame=CGRectMake(20, 210, 130, 80);
    email1.textColor=[UIColor redColor];
    email1.text=@"EMAIL";
    [self.view addSubview:email1];
    
    
    phonenumber1=[[UILabel alloc]init];
    phonenumber1.frame=CGRectMake(20,290, 150, 80);
    phonenumber1.textColor=[UIColor redColor];
    phonenumber1.text=@"PHONE NUMBER";
    [self.view addSubview:phonenumber1];
    
    
    password1=[[UILabel alloc]init];
    password1.frame=CGRectMake(20, 370, 150, 80);
    password1.textColor=[UIColor redColor];
    password1.text=@"PASSWRD";
    [self.view addSubview:password1];
    
    
    confirmpassword1=[[UILabel alloc]init];
    confirmpassword1.frame=CGRectMake(20, 450, 180, 80);
    confirmpassword1.textColor=[UIColor redColor];
    confirmpassword1.text=@"CONFERMPASSWRD";
    [self.view addSubview:confirmpassword1];
    
    
    firstname=[[UITextField alloc]init];
    firstname.frame=CGRectMake(180, 50, 180, 80);
    firstname.textColor=[UIColor redColor];
    firstname.placeholder=@"firstname";
    [self.view addSubview:firstname];

    lastname=[[UITextField alloc]init];
    lastname.frame=CGRectMake(180, 130, 180, 80);
    lastname.textColor=[UIColor redColor];
    lastname.placeholder=@"lastname";
    [self.view addSubview:lastname];
    
    email=[[UITextField alloc]init];
    email.frame=CGRectMake(180, 210, 180, 80);
    email.textColor=[UIColor redColor];
    email.placeholder=@"email";
    [self.view addSubview:email];
    
    phonenumber=[[UITextField alloc]init];
    phonenumber.frame=CGRectMake(180, 290, 180, 80);
    phonenumber.textColor=[UIColor redColor];
    phonenumber.placeholder=@"phonenumber";
    [self.view addSubview:phonenumber];
    
    password=[[UITextField alloc]init];
    password.frame=CGRectMake(180, 370, 180, 80);
    password.textColor=[UIColor redColor];
    password.placeholder=@"passwrd";
    [self.view addSubview:password];
    
    confirmpassword=[[UITextField alloc]init];
    confirmpassword.frame=CGRectMake(200, 450, 180, 80);
    confirmpassword.textColor=[UIColor redColor];
    confirmpassword.placeholder=@"cnfrmpswrd";
    [self.view addSubview:confirmpassword];
    
    button1=[[UIButton alloc]init];
    button1.frame=CGRectMake(150, 550, 100, 80);
    button1.backgroundColor=[UIColor blueColor];
    [button1 addTarget:self  action:@ selector(button1:) forControlEvents:UIControlEventTouchUpInside];
    [button1 setTitle:@"SUBMIT" forState:UIControlStateNormal];
      [self.view addSubview:button1];
    
}

-(void)button1:(UIButton*)sender
{
    UIAlertView* alert =[[UIAlertView alloc]initWithTitle:@"ok" message:@"enter the fields" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles: nil];
    if(firstname.text.length==0)
    {
        [alert show];
    }
    
    
   else if(lastname.text.length==0)
    {
        [alert show];
}
    
   else if(phonenumber.text.length==0)
    {
        [alert show];
    }
    
    
    
    
    
   else   if(email.text.length==0)
   {
       [alert show];
   }

 else   if(password.text.length==0)
    {
        [alert show];
    }

    
    
    
   else if(confirmpassword.text.length==0)
    {
        [alert show];
    }
}

-(BOOL)firstName:(NSString*)text {

    NSCharacterSet *set=[[NSCharacterSet characterSetWithCharactersInString:@"1234567890-+%"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location==NSNotFound )
    {
        NSLog(@"no special character");
        return  NO;
        
    }
    
    else{
        NSLog(@"has special character");
        return YES;
    }
}



- (void) Alertt1  {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"alert" message:@"only alphabats are allowed"
        preferredStyle:UIAlertControllerStyleAlert];
        
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    
    
                                        [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];

}




-(BOOL)lastName:(NSString*)text {
    
    NSCharacterSet *set=[[NSCharacterSet characterSetWithCharactersInString:@"1234567890-+%"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location==NSNotFound )
    {
        NSLog(@"no special character");
        return  NO;
        
    }
    
    else{
        NSLog(@"has special character");
        return YES;
    }
}
    - (void) Alertt2  {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"alert" message:@"only alphabats are allowed"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
 
    
    
    
    
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
- (void) Alertt3 {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"alert" message:@"special character require"
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    


}


    -(void)validatephonenumber;
{
NSString *phoneNumber;
NSString *phoneRegex = @"[235689][0-9]{6}([0-9]{3})?";
NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
BOOL matches = [test evaluateWithObject:phoneNumber];

}

- (void) Alertt4 {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"alert" message:@"only integer allowed"
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}



- (void) Alertt5 {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"alert" message:@"wrong passwrd"
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (void) Alertt6 {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"alert" message:@"passwrd mismatch"
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}



    


    // Do any additional setup after loading the view, typically from a nib.

 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
